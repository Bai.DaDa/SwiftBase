# SwiftBase

[![CI Status](https://img.shields.io/travis/baibingtao/SwiftBase.svg?style=flat)](https://travis-ci.org/baibingtao/SwiftBase)
[![Version](https://img.shields.io/cocoapods/v/SwiftBase.svg?style=flat)](https://cocoapods.org/pods/SwiftBase)
[![License](https://img.shields.io/cocoapods/l/SwiftBase.svg?style=flat)](https://cocoapods.org/pods/SwiftBase)
[![Platform](https://img.shields.io/cocoapods/p/SwiftBase.svg?style=flat)](https://cocoapods.org/pods/SwiftBase)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftBase is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SwiftBase'
```

## Author

baibingtao, baibingtao@smartlink.com.cn

## License

SwiftBase is available under the MIT license. See the LICENSE file for more info.
