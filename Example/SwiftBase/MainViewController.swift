//
//  MainViewController.swift
//  SwiftBase_Example
//
//  Created by 巴图 on 2023/2/9.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import UIKit
import SwiftBase

class MainViewController: SwiftBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
    }
}
