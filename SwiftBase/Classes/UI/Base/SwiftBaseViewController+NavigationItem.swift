//
//  SwiftBaseViewController+NavigationItem.swift
//  SwiftBase
//
//  Created by 巴图 on 2023/2/9.
//  SwiftBaseViewController 导航栏按钮 拓展

import Foundation
import UIKit

extension SwiftBaseViewController {
    
    /// 导航栏左侧返回按钮
    /// - Parameter image: 返回按钮图片
    @objc open func setNavBackButton(image: UIImage) {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.setImage(image, for: .selected)
        button.sizeToFit()
        button.addTarget(self, action: #selector(onBackButtonTap), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    /// 导航栏左侧返回按钮点击事件
    @objc open func onBackButtonTap() {
        guard let nav = navigationController else {
            dismiss(animated: true, completion: nil)
            return
        }
        if nav.viewControllers.count > 1 {
            nav.popViewController(animated: true)
        } else if let _ = nav.presentingViewController {
            nav.dismiss(animated: true, completion: nil)
        }
    }
    
    /// 导航栏右侧按钮（图片）
    /// - Parameters:
    ///   - normalImage: 正常状态下的图片
    ///   - highlightedImage: 高亮状态下的图片（默认不传）
    ///   - selectedImage: 选择状态下的图片（默认不传）
    ///   - target: 执行者
    ///   - action: 事件
    ///   - controlEvents: 手势
    @objc open func setNavRightButtonForImage(_ normalImage: UIImage?,
                                              highlightedImage: UIImage? = nil,
                                              selectedImage: UIImage? = nil,
                                              target: Any?,
                                              action: Selector,
                                              for controlEvents: UIControl.Event = .touchUpInside) {
        let button = UIButton(type: .custom)
        button.setImage(normalImage, for: .normal)
        if let highlightedImage = highlightedImage {
            button.setImage(highlightedImage, for: .highlighted)
        } else {
            button.setImage(normalImage, for: .highlighted)
        }
        if let selectedImage = selectedImage {
            button.setImage(selectedImage, for: .selected)
        } else {
            button.setImage(normalImage, for: .selected)
        }
        button.sizeToFit()
        if let target = target {
            button.addTarget(target, action: action, for: controlEvents)
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    /// 导航栏右侧按钮（文字）
    /// - Parameters:
    ///   - normalText: 正常状态下的文字
    ///   - normalTextColor: 正常状态下的文字颜色
    ///   - highlightedText: 高亮状态下的文字
    ///   - highlightedTextColor: 高亮状态下的文字颜色
    ///   - selectedText: 选择状态下的文字
    ///   - selectedTextColor: 选择状态下的文字颜色
    ///   - disabledText: 禁止状态下的文字
    ///   - disabledTextColor: 禁止状态下的文字颜色
    ///   - font: 字体
    ///   - target: 执行者
    ///   - action: 事件
    ///   - controlEvents: 手势
    @objc open func setNavRightButtonForText(_ normalText: String,
                                             normalTextColor: UIColor = UIColor(hex: 0x393C43),
                                             highlightedText: String = "",
                                             highlightedTextColor: UIColor = UIColor(hex: 0x393C43),
                                             selectedText: String = "",
                                             selectedTextColor: UIColor = UIColor(hex: 0x393C43),
                                             disabledText: String = "",
                                             disabledTextColor: UIColor = UIColor(hex: 0x393C43),
                                             font: UIFont = UIFont.systemFont(ofSize: 14.0, weight: .semibold),
                                             target: Any?,
                                             action: Selector,
                                             for controlEvents: UIControl.Event = .touchUpInside) {
        let button = UIButton(type: .custom)
        button.setTitle(normalText, for: .normal)
        button.setTitleColor(normalTextColor, for: .normal)
        if highlightedText.isEmpty {
            button.setTitle(normalText, for: .highlighted)
        } else {
            button.setTitle(highlightedText, for: .highlighted)
        }
        button.setTitleColor(highlightedTextColor, for: .highlighted)
        if selectedText.isEmpty {
            button.setTitle(normalText, for: .selected)
        } else {
            button.setTitle(selectedText, for: .selected)
        }
        button.setTitleColor(selectedTextColor, for: .selected)
        if disabledText.isEmpty {
            button.setTitle(normalText, for: .disabled)
        } else {
            button.setTitle(disabledText, for: .disabled)
        }
        button.setTitleColor(disabledTextColor, for: .disabled)
        button.titleLabel?.font = font
        button.sizeToFit()
        if let target = target {
            button.addTarget(target, action: action, for: controlEvents)
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    /// 导航栏右侧按钮（自定义 View）
    /// - Parameters:
    ///   - customView: 自定义 View
    ///   - target: 执行者
    ///   - action: 事件
    ///   - controlEvents: 手势
    @objc open func setNavRightButtonForCustomView(_ customView: UIView,
                                                   target: Any?,
                                                   action: Selector,
                                                   for controlEvents: UIControl.Event = .touchUpInside) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        customView.addGestureRecognizer(tap)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: customView)
    }
}
