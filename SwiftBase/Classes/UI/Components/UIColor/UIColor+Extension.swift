//
//  UIColor+Extension.swift
//  SwiftBase
//
//  Created by 巴图 on 2023/2/8.
//  UIColor 拓展

import Foundation
import UIKit

public extension UIColor {
    
    /// 随机颜色
    static var randomColor: UIColor {
        return UIColor(r: CGFloat(arc4random() % 256),
                       g: CGFloat(arc4random() % 256),
                       b: CGFloat(arc4random() % 256),
                       alpha: 1.0)
    }
    
    /// 十进制颜色
    /// - Parameters:
    ///   - r: 十进制 Red 值
    ///   - g: 十进制 Green 值
    ///   - b: 十进制 Blue 值
    ///   - alpha: 透明度
    convenience init(r: CGFloat,
                     g: CGFloat,
                     b: CGFloat,
                     alpha: CGFloat = 1.0) {
        self.init(red: r / 255.0,
                  green: g / 255.0,
                  blue: b / 255.0,
                  alpha: alpha)
    }
    
    /// 十六进制颜色
    /// - Parameters:
    ///   - hex: 十六进制色值
    ///   - alpha: 透明度
    convenience init(hex: Int,
                     alpha: CGFloat = 1.0) {
        self.init(red: (CGFloat)((hex & 0xFF0000) >> 16) / 255.0,
                  green: (CGFloat)((hex & 0xFF00) >> 8) / 255.0,
                  blue: (CGFloat)(hex & 0xFF) / 255.0,
                  alpha: alpha)
    }
    
    /// 十六进制颜色（字符串）
    /// - Parameters:
    ///   - hex: 十六进制色值（字符串）
    ///   - alpha: 透明度
    convenience init?(hexString: String,
                      alpha: CGFloat = 1.0) {
        let color = Self.hexStringToRGB(hexString: hexString)
        guard let r = color.r,
              let g = color.g,
              let b = color.b else {
                  return nil
              }
        self.init(r: r, g: g, b: b, alpha: alpha)
    }
    
    /// 根据十六进制色值（字符串）获取 RGB，如：#3CB371 或者 ##3CB371 -> 60,179,113
    /// - Parameter hexString: 十六进制色值（字符串），如：#3CB371 或者 ##3CB371
    /// - Returns: RGB 色值
    static func hexStringToRGB(hexString: String) -> (r: CGFloat?, g: CGFloat?, b: CGFloat?) {
        guard hexString.count >= 6 else {
            return (nil, nil, nil)
        }
        
        var tempHex = hexString.uppercased()
        if tempHex.hasPrefix("0x") || tempHex.hasPrefix("##") {
            tempHex = String(tempHex[tempHex.index(tempHex.startIndex, offsetBy: 2) ..< tempHex.endIndex])
        }
        if tempHex.hasPrefix("#") {
            tempHex = String(tempHex[tempHex.index(tempHex.startIndex, offsetBy: 1) ..< tempHex.endIndex])
        }
        
        var range = NSRange(location: 0, length: 2)
        let rHex = (tempHex as NSString).substring(with: range)
        range.location = 2
        let gHex = (tempHex as NSString).substring(with: range)
        range.location = 4
        let bHex = (tempHex as NSString).substring(with: range)
        
        var r: UInt32 = 0, g: UInt32 = 0, b: UInt32 = 0
        Scanner(string: rHex).scanHexInt32(&r)
        Scanner(string: gHex).scanHexInt32(&g)
        Scanner(string: bHex).scanHexInt32(&b)
        return (r: CGFloat(r), g: CGFloat(g), b: CGFloat(b))
    }
}
